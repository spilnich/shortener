package com.javarush.task.task33.task3310.tests;

import com.javarush.task.task33.task3310.Helper;
import com.javarush.task.task33.task3310.Shortener;
import com.javarush.task.task33.task3310.strategy.HashBiMapStorageStrategy;
import com.javarush.task.task33.task3310.strategy.HashMapStorageStrategy;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class SpeedTest {

    public long getTimeToGetIds(Shortener shortener, Set<String> strings, Set<Long> ids) {
        Date dateStart = new Date();
        long start = dateStart.getTime();

        for (String string : strings) {
            ids.add(shortener.getId(string));
        }

        Date dateEnd = new Date();
        long end = dateEnd.getTime();

        return end - start;
    }

    public long getTimeToGetStrings(Shortener shortener, Set<Long> ids, Set<String> strings) {
        Date dateStart = new Date();
        long start = dateStart.getTime();

        for (Long id : ids) {
            strings.add(shortener.getString(id));
        }

        Date dateEnd = new Date();
        long end = dateEnd.getTime();

        return end - start;
    }

    @Test
    public void testHashMapStorage() {
        Shortener shortener1 = new Shortener(new HashMapStorageStrategy());
        Shortener shortener2 = new Shortener(new HashBiMapStorageStrategy());
        Set<String> origStrings = new HashSet<>();
        Set<String> resultStrings = new HashSet<>();
        Set<Long> resultIds = new HashSet<>();

        for (int i = 0; i < 10000; i++) {
            origStrings.add(Helper.generateRandomString());
        }

        long getIdsTimeFirstMap = getTimeToGetIds(shortener1, origStrings, resultIds);
        long getIdsTimeSecondMap = getTimeToGetIds(shortener2, origStrings, resultIds);

        Assert.assertTrue(getIdsTimeFirstMap > getIdsTimeSecondMap);

        long getStringsTimeFirstMap = getTimeToGetStrings(shortener1, resultIds, resultStrings);
        long getStringsTimeSecondMap = getTimeToGetStrings(shortener2, resultIds, resultStrings);

        Assert.assertEquals(getStringsTimeFirstMap, getStringsTimeSecondMap, 100);

    }
}
