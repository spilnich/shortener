package com.javarush.task.task33.task3310;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Helper {

    public static String generateRandomString() {
//        StringBuilder builder = new StringBuilder();
//        String alphabet = "abcdefghigklmnopqrstuvwxyz";
//
//        for (int i = 0; i < 10; i++) {
//            builder.append(ThreadLocalRandom.current().nextInt(9));
//        }
//        builder.append(alphabet.charAt(new Random().nextInt(26)));
//
//        return builder.toString();

        return new BigInteger(130, new SecureRandom()).toString(36);
    }

    public static void printMessage(String message) {
        System.out.println(message);
    }
}
