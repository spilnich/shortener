package com.javarush.task.task33.task3310;

import com.javarush.task.task33.task3310.strategy.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Solution {
    public static void main(String[] args) {
//        System.out.println(Helper.generateRandomString());
        testStrategy(new HashMapStorageStrategy(), 10000);
        testStrategy(new OurHashMapStorageStrategy(), 10000);
//        testStrategy(new FileStorageStrategy(), 1000);
        testStrategy(new OurHashBiMapStorageStrategy(), 10000);
        testStrategy(new HashBiMapStorageStrategy(), 10000);
        testStrategy(new DualHashBidiMapStorageStrategy(), 10000);
    }

    public static Set<Long> getIds(Shortener shortener, Set<String> strings) {
        Set<Long> ides = new HashSet<>();
        for (String string : strings) {
            ides.add(shortener.getId(string));
        }
        return ides;
    }

    public static Set<String> getStrings(Shortener shortener, Set<Long> keys) {
        Set<String> stringSet = new HashSet<>();
        for (Long key : keys) {
            stringSet.add(shortener.getString(key));
        }
        return stringSet;
    }

    public static void testStrategy(StorageStrategy strategy, long elementsNumber) {
        Helper.printMessage(strategy.getClass().getSimpleName());
        Shortener shortener = new Shortener(strategy);
        Set<String> strings = new HashSet<>();
        Set<Long> ides = new HashSet<>();
        for (long i = 0; i < elementsNumber; i++) {
            strings.add(Helper.generateRandomString());
            ides.add(i);
        }
        Date date = new Date();
        long start = date.getTime();
        getIds(shortener, strings);
        Date date2 = new Date();
        long end = date2.getTime();
        Helper.printMessage(String.valueOf(end - start));

        Date date3 = new Date();
        long start2 = date3.getTime();
        int amountOfStrings = getStrings(shortener, ides).size();
        Date date4 = new Date();
        long end2 = date4.getTime();
        Helper.printMessage(String.valueOf(end2 - start2));

        Helper.printMessage(amountOfStrings == strings.size() ? "Тест пройден.\n" : "Тест не пройден.");
    }
}
