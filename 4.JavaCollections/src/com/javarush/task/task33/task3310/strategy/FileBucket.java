package com.javarush.task.task33.task3310.strategy;

import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;

public class FileBucket {
    private Path path;

    public FileBucket() {
        try {
            path = Files.createTempFile("data", null);
            Files.createFile(path);
            Files.deleteIfExists(path);
            path.toFile().deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public long getFileSize() {
        try {
            return Files.size(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void remove() {
        try {
            Files.delete(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void putEntry(Entry entry) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(path));
            oos.writeObject(entry);
        } catch (IOException e) {
            e.printStackTrace();
        }
//
//
//        Files.walkFileTree(path, new SimpleFileVisitor<>() {
//            @Override
//            public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
//                return super.postVisitDirectory(dir, exc);
//            }
//        })
    }

    public Entry getEntry() {
        Entry entry = null;
        if (getFileSize() > 0) {
            entry = null;
            try {
                ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(path));
                entry = (Entry) ois.readObject();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return entry;
    }
}
