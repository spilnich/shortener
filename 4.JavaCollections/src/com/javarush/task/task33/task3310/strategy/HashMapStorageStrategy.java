package com.javarush.task.task33.task3310.strategy;

import java.util.*;

public class HashMapStorageStrategy implements StorageStrategy {
    private HashMap<Long, String> data = new HashMap<>();

    @Override
    public boolean containsKey(Long key) {
        return data.containsKey(key);
    }

    @Override
    public boolean containsValue(String value) {
        return data.containsValue(value);
    }

    @Override
    public void put(Long key, String value) {
        data.put(key, value);
    }

    @Override
    public Long getKey(String value) {
        Set<Long> keys = new HashSet<>();
        for (Map.Entry<Long, String> entry : data.entrySet()) {
            if (Objects.equals(value, entry.getValue())) {
                keys.add(entry.getKey());
            }
        }
        return !keys.isEmpty() ? keys.iterator().next() : null;
    }

    @Override
    public String getValue(Long key) {
        return data.get(key);
    }
}
