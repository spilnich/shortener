package com.javarush.task.task33.task3310.strategy;

import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.Map;

public class DualHashBidiMapStorageStrategy implements StorageStrategy {
    DualHashBidiMap data;

    public DualHashBidiMapStorageStrategy() {
        data = new DualHashBidiMap();
    }

    @Override
    public boolean containsKey(Long key) {
        return data.containsKey(key);
    }

    @Override
    public boolean containsValue(String value) {
//        BidiMap map = data.inverseBidiMap();
        return data.containsValue(value);
    }

    @Override
    public void put(Long key, String value) {
        data.put(key, value);
    }

    @Override
    public Long getKey(String value) {
        return (Long) data.getKey(value);
    }

    @Override
    public String getValue(Long key) {
        return (String) data.get(key);
    }
}
