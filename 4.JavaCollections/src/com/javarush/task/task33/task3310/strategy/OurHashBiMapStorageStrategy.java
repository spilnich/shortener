package com.javarush.task.task33.task3310.strategy;

import java.util.HashMap;

public class OurHashBiMapStorageStrategy implements StorageStrategy {
    private HashMap<Long, String> k2v;
    private HashMap<String, Long> v2k;

    public OurHashBiMapStorageStrategy() {
        k2v = new HashMap<>();
        v2k = new HashMap<>();
    }

    @Override
    public boolean containsKey(Long key) {
        return key != null && k2v.containsKey(key);

//        if (key == null) {
//            return false;
//        }
//        for (Long k : k2v.keySet()) {
//            if (k.equals(key)) {
//                return true;
//            }
//        }
//        return false;
    }

    @Override
    public boolean containsValue(String value) {
        return value != null && v2k.containsKey(value);
    }

    @Override
    public void put(Long key, String value) {
        k2v.put(key, value);
        v2k.put(value, key);
    }

    @Override
    public Long getKey(String value) {
        return v2k.get(value);
    }

    @Override
    public String getValue(Long key) {
        return k2v.get(key);
    }
}
